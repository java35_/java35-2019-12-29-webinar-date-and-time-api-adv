import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class PlusDateTime {
	public static void main(String[] args) {
		System.out.println(Instant.now().plus(5, ChronoUnit.DAYS));
		System.out.println(Instant.now().plus(Duration.ofDays(5)));
		System.out.println(Instant.now().plus(Period.ofDays(5)));
		
		System.out.println(Instant.parse("2019-10-27T00:00:00.0Z")
				.plus(4, ChronoUnit.HOURS));
		System.out.println(LocalDateTime.parse("2019-10-27T00:00:00.0")
				.plus(4, ChronoUnit.HOURS));
		System.out.println(ZonedDateTime.parse("2020-03-27T00:00:00.0+02:00[Asia/Jerusalem]")
				.plusHours(4));
	}
}