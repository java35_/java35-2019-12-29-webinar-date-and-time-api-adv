import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class GetDateTime {

	public static void main(String[] args) {
		// parse 
		System.out.println(Instant.parse("2016-01-01T00:00:00.000000Z"));
		System.out.println(LocalDate.parse("2016-01-01"));
		System.out.println(LocalTime.parse("12:00:00.000000"));
		System.out.println(LocalDateTime.parse("2016-01-01T00:00:00.000000"));
		System.out.println(ZonedDateTime.parse("2016-01-01T00:00:00.000000+02:00[Asia/Jerusalem]"));
	
		// constants
		System.out.println("\nConstants");
		
		System.out.println("Instant:");
		System.out.println(Instant.MIN);
		System.out.println(Instant.EPOCH);
		System.out.println(Instant.MAX);
		
		System.out.println("LocalDate:");
		System.out.println(LocalDate.MIN);
		System.out.println(LocalDate.EPOCH);
		System.out.println(LocalDate.MAX);

		System.out.println("LocalTime:");
		System.out.println(LocalTime.MIN);
		System.out.println(LocalTime.MAX);
		System.out.println(LocalTime.MIDNIGHT);
		System.out.println(LocalTime.NOON);
		
		System.out.println("LocalDateTime:");
		System.out.println(LocalDateTime.MAX);
		System.out.println(LocalDateTime.MIN);
//		System.out.println(ZonedDateTime.);
		
		// of
		System.out.println("\nof()");
		System.out.println("Instant:");
		System.out.println(Instant.ofEpochMilli(1));
		System.out.println(Instant.ofEpochSecond(1));
		System.out.println(Instant.ofEpochSecond(1, 1));
		
		System.out.println("LocalDate:");
		System.out.println(LocalDate.of(2019, 5, 25));
		System.out.println(LocalDate.of(2019, Month.NOVEMBER, 1));
		System.out.println(LocalDate.ofEpochDay(1));
		System.out.println(LocalDate.ofYearDay(2016, 60));
		System.out.println(LocalDate.ofInstant(Instant.now(), ZoneId.of("Australia/Sydney")));
		System.out.println(LocalDate.ofInstant(Instant.now(), ZoneId.of(ZoneId.SHORT_IDS.get("AET"))));
		System.out.println(ZoneId.systemDefault());
		
		System.out.println(ZoneId.systemDefault());
		System.out.println(LocalTime.ofNanoOfDay(1));
		System.out.println(LocalTime.ofSecondOfDay(1));
		
		System.out.println(LocalDateTime.of(LocalDate.now(), LocalTime.now()));
		System.out.println(LocalDateTime.ofEpochSecond(1, 1, ZoneOffset.ofHours(2)));
		System.out.println(LocalDateTime.ofInstant(Instant.now(), ZoneId.of("Australia/Sydney")));

		System.out.println(ZonedDateTime.of(LocalDateTime.now(), ZoneId.of("Australia/Sydney")));
		
		
	}
}