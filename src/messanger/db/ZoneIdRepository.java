package messanger.db;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ZoneIdRepository implements IZoneIdRepository {

	private List<MessageDao> messages = new ArrayList<>();
	
	@Override
	public List<MessageDao> getMessagesByPeriod(Instant start, Instant end) {
		return messages.stream()
				.filter(x -> x.getTime().isAfter(start))
				.filter(x -> x.getTime().isBefore(end))
				.collect(Collectors.toList());
	}

	@Override
	public void add(MessageDao message) {
		messages.add(message);
	}

}
