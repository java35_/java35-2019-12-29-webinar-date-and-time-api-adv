package messanger.db;

import java.time.Instant;

public class MessageDao {
	private String message;
	private Instant time;
	
	public MessageDao(String message, Instant time) {
		this.message = message;
		this.time = time;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Instant getTime() {
		return time;
	}
	public void setTime(Instant time) {
		this.time = time;
	}
}
