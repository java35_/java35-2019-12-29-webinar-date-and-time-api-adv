package messanger.db;

import java.time.Instant;
import java.util.List;

public interface IZoneIdRepository {

	List<MessageDao> getMessagesByPeriod(Instant start, Instant end);
	void add(MessageDao message);
}
