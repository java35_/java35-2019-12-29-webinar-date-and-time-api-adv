package messanger.ui;

import java.util.Scanner;

import messanger.service.IZoneIdService;

public class ZoneIdController {
	
	IZoneIdService service;
	
	public ZoneIdController(IZoneIdService service) {
		this.service = service;
	}

	public void start() {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter your timezone:");
		System.out.println("1 - Asia/Shanghai");
		System.out.println("2 - Australia/Sydney");
		
		String timeZone = sc.nextLine();
		
		System.out.println("Enter start date");
		String start = sc.nextLine();
		
		System.out.println("Enter end date");
		String end = sc.nextLine();
		
		service.getMessagesByPeriod(timeZone, start, end)
				.forEach(System.out::println);
		
	}
}
