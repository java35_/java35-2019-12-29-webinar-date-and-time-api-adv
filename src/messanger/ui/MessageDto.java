package messanger.ui;

import java.time.LocalDateTime;

public class MessageDto {
	String message;
	LocalDateTime time;
	
	public MessageDto(String message, LocalDateTime time) {
		this.message = message;
		this.time = time;
	}

	@Override
	public String toString() {
		return "Message: " + message + " Date/time: " + time;
	}
}
