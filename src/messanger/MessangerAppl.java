package messanger;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import messanger.db.IZoneIdRepository;
import messanger.db.MessageDao;
import messanger.db.ZoneIdRepository;
import messanger.service.IZoneIdService;
import messanger.service.ZoneIdService;
import messanger.ui.ZoneIdController;

public class MessangerAppl {

	public static void main(String[] args) {
		IZoneIdRepository repo = new ZoneIdRepository();
		
		for (int i = 0; i <= 5; i++) {
			repo.add(new MessageDao(Instant.now().plus(i, ChronoUnit.HOURS).toString(), 
					Instant.now().plus(i, ChronoUnit.HOURS)));
		}
		
		IZoneIdService service = new ZoneIdService(repo);
		ZoneIdController controller = new ZoneIdController(service);
		controller.start();
	}

}
