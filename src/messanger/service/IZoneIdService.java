package messanger.service;

import java.util.List;

import messanger.ui.MessageDto;

public interface IZoneIdService {

	List<MessageDto> getMessagesByPeriod(String timeZone, String start, String end);

}
