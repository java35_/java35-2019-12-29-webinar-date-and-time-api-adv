package messanger.service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

import messanger.db.IZoneIdRepository;
import messanger.db.MessageDao;
import messanger.ui.MessageDto;

public class ZoneIdService implements IZoneIdService {

	IZoneIdRepository repo;
	
	public ZoneIdService(IZoneIdRepository repo) {
		this.repo = repo;
	}

	
	// start 2019-08-29 (c 00:00:00)
	// end 2019-08-30 (�� 23:59:59.9999999)
	@Override
	public List<MessageDto> getMessagesByPeriod(String timeZone, String start, String end) {
		Instant startInstant;
		Instant endInstant;
		
		LocalDateTime startLocalDateTime = 
				LocalDateTime.of(LocalDate.parse(start), LocalTime.MIN);
		
		LocalDateTime endLocalDateTime = 
				LocalDateTime.of(LocalDate.parse(end), LocalTime.MAX);
		
		ZoneId zone = null;
		if (timeZone.equals("1"))
			zone = ZoneId.of("Asia/Shanghai");
		else 
			zone = ZoneId.of("Australia/Sydney");
		startInstant = ZonedDateTime.of(startLocalDateTime, zone).toInstant();
		endInstant = ZonedDateTime.of(endLocalDateTime, zone).toInstant();
		
		List<MessageDao> messages = repo.getMessagesByPeriod(startInstant, endInstant);
		
		ZoneId[] zones = {zone};
		
		// ������������ List<MessageDao> -> List<MessageDto>
		return messages.stream()
				.map(x -> cnvMessageDaoToDto(x, zones[0]))
				.collect(Collectors.toList());
	}


	private MessageDto cnvMessageDaoToDto(MessageDao messageDao, ZoneId zone) {
		LocalDateTime time = 
				ZonedDateTime.ofInstant(messageDao.getTime(), zone).toLocalDateTime();
		return new MessageDto(messageDao.getMessage(), time);
	}

}
